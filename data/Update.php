<?php
class Update{
	
	public static function updateData($table, $conn, $values, $where){
		$val = "";
		$wh = "";
		foreach($values as $key=>$value){
			$val = $val . $key . " = " . "'".$value."'" . ", "  ; 
		}
		foreach($where as $key=>$value){
			$wh = $wh . " AND " . $key . " " . "'".$value."'"  ; 
		}
		$val = strip_tags($val);
		$val = trim($val, ", ");
		$query = "UPDATE ". $table ." SET ". $val . " WHERE 1=1 " . $wh;
		// array for JSON response
		$response = array();
		$flag['code']=0;
		// mysql inserting a new row
		mysql_query("SET NAMES 'utf8'");
		mysql_query("SET CHARACTER SET 'utf8'");
		$result = mysql_query($query, $conn);
        if($result){
            $flag['code']=1;
        }
        print(json_encode($flag));
	}
    
    public static function updateCustom($table, $conn, $set){
        $query = "UPDATE ". $table ." SET ". $set;
		// array for JSON response
		$response = array();
		$flag['code']=0;
		// mysql inserting a new row
		mysql_query("SET NAMES 'utf8'");
		mysql_query("SET CHARACTER SET 'utf8'");
		$result = mysql_query($query, $conn);
        if($result){
            $flag['code']=1;
        }
        print(json_encode($flag));
    }
}

?>