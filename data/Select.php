<?php
class Select{
	
	public static function selectAll($table, $conn){
		$query = "SELECT * FROM " . $table;
		$getData = mysql_query($query, $conn);

		if(!$getData){
			die('Could not get data: ' . mysql_error());
		}
		$rows = array();
		while($row = mysql_fetch_array($getData, MYSQL_ASSOC)){
			$rows[] = $row;
		}
	        
		print PrintJSON::prettyPrint( json_encode($rows, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT) );
	}
	
	public static function selectVar($table, $conn, $values){
		$val = "";
		foreach($values as $value){
			$val = $val . $value . ", "; 
		}
		$val = strip_tags($val);
		$val = trim($val, ", ");
		$query = "SELECT ". $val ." FROM " . $table;
		$getData = mysql_query($query, $conn);

		if(!$getData){
			die('Could not get data: ' . mysql_error());
		}
		$rows = array();
		while($row = mysql_fetch_array($getData, MYSQL_ASSOC)){
			$rows[] = $row;
		}
	        
		print PrintJSON::prettyPrint( json_encode($rows, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT) );
	}
	
	public static function selectWhere($table, $conn, $values, $where){
		$val = "";
		$wh = "";
		foreach($values as $value){
			$val = $val . $value . ", "; 
		}
		foreach($where as $key=>$value){
			$wh = $wh . " AND " . $key . " " . "'".$value."'"  ; 
		}
		$val = strip_tags($val);
		$val = trim($val, ", ");
		
		$query = "SELECT ". $val ." FROM " . $table . " WHERE 1=1 " . $wh;
		$getData = mysql_query($query, $conn);

		if(!$getData){
			die('Could not get data: ' . mysql_error());
		}
		$rows = array();
		while($row = mysql_fetch_array($getData, MYSQL_ASSOC)){
			$rows[] = $row;
		}
	        
		print PrintJSON::prettyPrint( json_encode($rows, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT) );
	}
    
    public static function selectCustom($table, $conn, $query){
        $getData = mysql_query($query, $conn);

		if(!$getData){
			die('Could not get data: ' . mysql_error());
		}
		$rows = array();
		while($row = mysql_fetch_array($getData, MYSQL_ASSOC)){
			$rows[] = $row;
		}
	        
		print PrintJSON::prettyPrint( json_encode($rows, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT) );
    }
		
}

?>