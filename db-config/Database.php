<?php
class Database {
	
	private $host;
	private $user;
	private $pass;
	private $database;
	public $conn;
	
	public function __construct($host, $user, $pass, $database){
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->database = $database;
	}
	
	public function openConnection(){
		#Open Connection
		$this->conn = mysql_connect($this->host, $this->user, $this->pass) or die("Error:".mysql_error());
		mysql_select_db($this->database);
		mysql_query("SET NAMES 'utf8'");
		mysql_query("SET CHARACTER SET 'utf8'");
	}
	
	public function closeConnection(){
		mysql_close($this->conn);
	}
	
	public function getConn(){
		return $this->conn;
	}
	
	
}


?>