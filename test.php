<?php
include('db-config/Database.php');
include('db-config/Config.php');
include('PrintJSON.php');
include('data/Select.php');
include('data/Update.php');

$database = new Database(Config::$host, Config::$user, Config::$pass, Config::$db);
$database->openConnection();

$values = array("CATEGORY", "PRODNAME", "PRICE");
$where = array("USERNAME =" => "REDONE");


$database->closeConnection();

/*
##########SELECT CLASS############

select all values from the database 
@var1 = Database Table;
@var2 = Database Connection;
use -> Select::selectAll(var1, var2);

select some values from the database 
@var1 = Database Table;
@var2 = Database Connection;
@var3 = values array;
use -> Select::selectVar(var1, var2, var3);

select values from the database with where clause
@var1 = Database Table;
@var2 = Database Connection;
@var3 = values array;
@var4 = key value array(SELECT * FROM TABLE WHERE 1=1 AND key = value;
use -> Select::selectWhere(var1, var2, var3, var4);
::NOTE:: 
you should put the operator after the key value
for example $where = array("USERNAME =" => "REDONE");
or $where = array("USERNAME <=" => "1");

Json data from a custom cuery
@var1 = Database Table;
@var2 = Database Connection;
@var3 = query;
Select::selectCustom(var1, va2, var3);


##########UPDATE CLASS############

update data on database
@var1 = Database Table;
@var2 = Database Connection;
@var3 = key value array. As SET KEY = "VALUE";
@var4 = key value array(SELECT * FROM TABLE WHERE 1=1 AND key = value;
Update::updateData(var1, var2, var3, var4);

update data with custom query after SET claus
@var1 = Database Table;
@var2 = Database Connection;
@var3 = after SET query;
Update::updateData(var1, var2, var3);

*/
?>